package world.map;

import com.engine.display.DisplayWindow;
import com.engine.display.DrawingArea;
import com.engine.display.WindowMeta;
import com.engine.input.MouseMeta;
import parser.ParseFile;
import world.map.traversal.TraversalFlag;
import world.map.traversal.TraversalManager;
import world.map.traversal.path.Path;
import world.map.traversal.path.PathFinder;
import world.map.traversal.path.pathfinder.BFSPathfinder;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.EnumSet;

import static world.map.traversal.TraversalFlag.*;

public class TileMap extends DisplayWindow {

    private TraversalManager traversalManager;
    private PathFinder pathFinder;

    private int viewWidth, viewHeight;
    private int width, height;
    private int shiftX, shiftY;
    private int dragX, dragY;

    private int tileSize = 10;

    private Path path;
    private Position origin;
    private Position target;
    private final BufferedImage testImg = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    private int[] pix;

    public TileMap(int mapWidth, int mapHeight) {
        super(new WindowMeta.Builder() {{
            setTitle("Path Finder");
            setWidth(mapWidth * 10);
            setHeight(mapHeight * 10);
        }}.build());
        viewWidth = mapWidth;
        viewHeight = mapHeight;

        width = 104; height = 104;
        traversalManager = new TraversalManager(width, height);
        new ParseFile(traversalManager.getMatrix()).parse();

//        width = 40; height = 30;
//        traversalManager = new TraversalManager(width, height);
//        MazeGenerator.generate(traversalManager.getMatrix());

        pathFinder = new BFSPathfinder(traversalManager);

        pix = ((DataBufferInt) testImg.getRaster().getDataBuffer()).getData();
        drawingArea.focus(pix, new Insets(0, 0, 200, 200));
    }

    DrawingArea drawingArea = new DrawingArea();
    double rad = 0;

    @Override
    public void update(double dt) {
        if (isKeyDown(KeyEvent.VK_L)) {
//            rad += Math.PI / 4 / 64;
            rad = Math.PI / 4;
//            rad = 0;
            drawingArea.fill(DrawingArea.color(25, 25, 25));
            drawingArea.fillRect(85, 85, 30, 30, DrawingArea.color(255, 0, 255), rad);
        }

        if (isKeyDown(KeyEvent.VK_M)) {
//            MazeGenerator.generate(traversalManager.getMatrix());
            tileSize = 26;
            dragX = 442;
            dragY = 1586;
            viewWidth = getMeta().getWidth() / tileSize;
            viewHeight = getMeta().getHeight() / tileSize;
            correctView();
        }

        if (isKeyDown(KeyEvent.VK_PLUS) && tileSize < 60) {
            tileSize += 2;
            viewWidth = getMeta().getWidth() / tileSize;
            viewHeight = getMeta().getHeight() / tileSize;
            dragX += viewWidth * 2;
            dragY += viewHeight * 2;
            correctView();
        }

        if (isKeyDown(KeyEvent.VK_MINUS) && tileSize > 6) {
            tileSize -= 2;
            dragX -= viewWidth * 2;
            dragY -= viewHeight * 2;
            viewWidth = getMeta().getWidth() / tileSize;
            viewHeight = getMeta().getHeight() / tileSize;
            correctView();
        }

        if (!isPressType(MouseMeta.RELEASED) || getLastWheelTick() != 0) {
            if (getLastWheelTick() != 0) {
                dragY += getLastWheelTick() * tileSize;
            } else {
                dragX -= getDragX();
                dragY -= getDragY();
            }

            correctView();
        }

        if (!isClickType(MouseMeta.RELEASED)) {
            int x = getPressedX() / tileSize + shiftX;
            int y = getPressedY() / tileSize + shiftY;

            if (x < 0 || y < 0 || x >= width || y >= height) {
                return;
            }

            if (isKeyDown(KeyEvent.VK_CONTROL)) {
                if (isClickType(MouseMeta.LEFT_CLICK)) {
                    target = Position.create(x, y);
                } else if (isClickType(MouseMeta.RIGHT_CLICK)) {
                    origin = Position.create(x, y);
                }

                if (origin != null && target != null) {
                    path = pathFinder.findPath(origin, target, 1);
                }
            }

            if (isKeyDown(KeyEvent.VK_SHIFT)) {
                EnumSet<TraversalFlag> traversable = EnumSet.noneOf(TraversalFlag.class);

                for (TraversalFlag value : values()) {
                    if (traversalManager.flagged(x, y, value)) {
                        traversable.add(value);
                    }
                }

                System.out.printf("(x, y) -> (%d, %d)\n", x, y);
                System.out.println(Integer.toBinaryString(traversalManager.getMatrix().get(x, y)));

                if (!traversable.isEmpty() || traversalManager.getMatrix().get(x, y) != 0) {
                    System.out.println("Flags: " + traversable);

                    String binString = Integer.toBinaryString(traversalManager.getMatrix().get(x, y));
                    char[] formatted = "0b0000_0000_0000_0000_0000_0000_0000_0000".toCharArray();

                    for (int index = formatted.length - 1, str = binString.length() - 1; str >= 0; index--) {
                        if (formatted[index] != '_') {
                            formatted[index] = binString.charAt(str--);
                        }
                    }

                    System.out.println("Raw: " + new String(formatted));
                }
            }
        }
    }

    private void correctView() {
        if (dragX > tileSize * (width - viewWidth)) {
            dragX = tileSize * (width - viewWidth);
        }

        if (dragY > tileSize * (height - viewHeight)) {
            dragY = tileSize * (height - viewHeight);
        }

        if (dragX < 0) {
            dragX = 0;
        }

        if (dragY < 0) {
            dragY = 0;
        }

        if (dragX / tileSize != shiftX) {
            shiftX = dragX / tileSize;

            if (shiftX > width - viewWidth) {
                shiftX = width - viewWidth;
            }
        }

        if (dragY / tileSize != shiftY) {
            shiftY = dragY / tileSize;

            if (shiftY > height - viewHeight) {
                shiftY = height - viewHeight;
            }
        }
    }

    @Override
    public void render(Graphics2D graphics, double remainingFrames) {

        if (drawingArea != null) {
            graphics.drawImage(testImg, 35, 35, null);
            return;
        }

        if (path != null && path.isPossible()) {
            int idx = 0;
            int size = path.getPath().size();
            for (Position position : path.getPath()) {
                int r = 255 * (size - idx) / size & 0xFF;
                int g = 255 * idx / size & 0xFF;
                idx++;
                graphics.setColor(new Color(127 << 24 | r << 16 | g << 8, true));
                graphics.fillRect(shiftedXTile(position.getX()), shiftedYTile(position.getY()), tileSize, tileSize);
            }
        }

        if (origin != null) {
            graphics.setColor(Color.PINK);
            graphics.fillRect(shiftedXTile(origin.getX()), shiftedYTile(origin.getY()), tileSize, tileSize);
        }

        if (target != null) {
            graphics.setColor(PURPLE);
            graphics.fillRect(shiftedXTile(target.getX()), shiftedYTile(target.getY()), tileSize, tileSize);
        }

        int length = viewWidth * viewHeight;

        graphics.setColor(Color.DARK_GRAY);
        for (int index = 0; index < length; index++) {
            int x = index % viewWidth;
            int y = index / viewWidth;
            graphics.drawRect(tileSize * x, tileSize * y, tileSize, tileSize);
        }

        for (int index = 0; index < length; index++) {
            int x = shiftX + index % viewWidth;
            int y = shiftY + index / viewWidth;

            graphics.setColor(Color.RED);
            drawTraversal(graphics, x, y);

            graphics.setColor(Color.LIGHT_GRAY);
            drawProjectile(graphics, x, y);
        }
    }

    private static final Color PURPLE = new Color(0xAA00AA);

    private void drawTraversal(Graphics2D graphics, int x, int y) {
        graphics.setColor(Color.BLUE);

        if (traversalManager.flagged(x, y, LANDSCAPE_BLOCKED)) {
            int dx = shiftedXTile(x + 0.4);
            int dy = shiftedYTile(y + 0.4);
            graphics.fillRect(dx, dy, tileSize / 4, tileSize / 4);
        }

        graphics.setColor(Color.RED);
        if (traversalManager.flagged(x, y, WALK_BLOCKED)) {
            int dx = shiftedXTile(x + 0.4);
            int dy = shiftedYTile(y + 0.4);
            graphics.fillRect(dx, dy, tileSize / 4, tileSize / 4);
        }

        if (traversalManager.flagged(x, y, WALK_NORTH_WEST)) {
            int dx = shiftedXTile(x);
            int dy = shiftedYTile(y);
            graphics.fillRect(dx, dy, tileSize / 4, tileSize / 4);
        }

        if (traversalManager.flagged(x, y, WALK_NORTH)) {
            int dx = shiftedXTile(x);
            int dy = shiftedYTile(y);
            graphics.drawLine(dx, dy + 1, dx + tileSize, dy + 1);
        }

        if (traversalManager.flagged(x, y, WALK_NORTH_EAST)) {
            int dx = shiftedXTile(x + 0.75);
            int dy = shiftedYTile(y);
            graphics.fillRect(dx, dy, tileSize / 4, tileSize / 4);
        }

        if (traversalManager.flagged(x, y, WALK_EAST)) {
            int dx = shiftedXTile(x + 1);
            int dy = shiftedYTile(y);
            graphics.drawLine(dx - 1, dy, dx - 1, dy + tileSize);
        }

        if (traversalManager.flagged(x, y, WALK_SOUTH_EAST)) {
            int dx = shiftedXTile(x + 0.75);
            int dy = shiftedYTile(y + 0.75);
            graphics.fillRect(dx, dy, tileSize / 4, tileSize / 4);
        }

        if (traversalManager.flagged(x, y, WALK_SOUTH)) {
            int dx = shiftedXTile(x);
            int dy = shiftedYTile(y + 1);
            graphics.drawLine(dx, dy - 1, dx + tileSize, dy - 1);
        }

        if (traversalManager.flagged(x, y, WALK_SOUTH_WEST)) {
            int dx = shiftedXTile(x);
            int dy = shiftedYTile(y + 0.75);
            graphics.fillRect(dx, dy, tileSize / 4, tileSize / 4);
        }

        if (traversalManager.flagged(x, y, WALK_WEST)) {
            int dx = shiftedXTile(x);
            int dy = shiftedYTile(y);
            graphics.drawLine(dx + 1, dy, dx + 1, dy + tileSize);
        }
    }

    private void drawProjectile(Graphics2D graphics, int x, int y) {
        if (traversalManager.flagged(x, y, PROJECTILE_BLOCKED)) {
            int dx = shiftedXTile(x + 0.4);
            int dy = shiftedYTile(y + 0.4);
            graphics.fillRect(dx + 1, dy + 1, tileSize / 4 - 2, tileSize / 4 - 2);
        }

        if (traversalManager.flagged(x, y, PROJECTILE_NORTH_WEST)) {
            int dx = shiftedXTile(x);
            int dy = shiftedYTile(y);
            graphics.fillRect(dx + 1, dy + 1, tileSize / 4 - 2, tileSize / 4 - 2);
        }

        if (traversalManager.flagged(x, y, PROJECTILE_NORTH)) {
            int dx = shiftedXTile(x);
            int dy = shiftedYTile(y);
            graphics.drawLine(dx, dy, dx + tileSize, dy);
        }

        if (traversalManager.flagged(x, y, PROJECTILE_NORTH_EAST)) {
            int dx = shiftedXTile(x + 0.75);
            int dy = shiftedYTile(y);
            graphics.fillRect(dx + 1, dy + 1, tileSize / 4 - 2, tileSize / 4 - 2);
        }

        if (traversalManager.flagged(x, y, PROJECTILE_EAST)) {
            int dx = shiftedXTile(x + 1);
            int dy = shiftedYTile(y);
            graphics.drawLine(dx, dy, dx, dy + tileSize);
        }

        if (traversalManager.flagged(x, y, PROJECTILE_SOUTH_EAST)) {
            int dx = shiftedXTile(x + 0.75);
            int dy = shiftedYTile(y + 0.75);
            graphics.fillRect(dx + 1, dy + 1, tileSize / 4 - 2, tileSize / 4 - 2);
        }

        if (traversalManager.flagged(x, y, PROJECTILE_SOUTH)) {
            int dx = shiftedXTile(x);
            int dy = shiftedYTile(y + 1);
            graphics.drawLine(dx, dy, dx + tileSize, dy);
        }

        if (traversalManager.flagged(x, y, PROJECTILE_SOUTH_WEST)) {
            int dx = shiftedXTile(x);
            int dy = shiftedYTile(y + 0.75);
            graphics.fillRect(dx + 1, dy + 1, tileSize / 4 - 2, tileSize / 4 - 2);
        }

        if (traversalManager.flagged(x, y, PROJECTILE_WEST)) {
            int dx = shiftedXTile(x);
            int dy = shiftedYTile(y);
            graphics.drawLine(dx, dy, dx, dy + tileSize);
        }
    }

    private int shiftedXTile(double x) {
        return (int) ((x - shiftX) * tileSize);
    }

    private int shiftedYTile(double y) {
        return (int) ((y - shiftY) * tileSize);
    }

}
