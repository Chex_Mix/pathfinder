package world.map;

public final class CoordinateOutOfBoundsException extends RuntimeException {

    public CoordinateOutOfBoundsException(String message) {
        super(message);
    }

}
