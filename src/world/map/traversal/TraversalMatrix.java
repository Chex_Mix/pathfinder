package world.map.traversal;

import world.map.CoordinateOutOfBoundsException;

import java.util.Arrays;

public class TraversalMatrix {
    private final int[] flags;
    private final int width, height;

    public TraversalMatrix(int width, int height) {
        this.flags = new int[width * height];
        this.width = width;
        this.height = height;
    }

    public void flagAll(int x, int y, TraversalFlag... flags) {
        for (TraversalFlag flag : flags) {
            flag(x, y, flag.getFlag());
        }
    }

    public void unflagAll(int x, int y, TraversalFlag... flags) {
        for (TraversalFlag flag : flags) {
            unflag(x, y, flag.getFlag());
        }
    }

    public boolean anyOf(int x, int y, TraversalFlag... flags) {
        for (TraversalFlag flag : flags) {
            if (flagged(x, y, flag.getFlag())) {
                return true;
            }
        }
        return false;
    }

    public void flag(int x, int y, TraversalFlag flag) {
        flag(x, y, flag.getFlag());
    }

    public void unflag(int x, int y, TraversalFlag flag) {
        unflag(x, y, flag.getFlag());
    }

    public boolean flagged(int x, int y, TraversalFlag flag) {
        return flagged(x, y, flag.getFlag());
    }

    public void flag(int x, int y, int flag) {
        checkBounds(x, y);
        flags[x + y * width] |= flag;
    }

    public void unflag(int x, int y, int flag) {
        checkBounds(x, y);
        flags[x + y * width] &= ~flag;
    }

    public void setAll(int flag) {
        Arrays.fill(flags, flag);
    }

    public boolean flagged(int x, int y, int flag) {
        return (get(x, y) & flag) == flag;
    }

    public int get(int x, int y) {
        checkBounds(x, y);
        return flags[x + y * width];
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public boolean inBounds(int x, int y) {
        return x >= 0 && y >= 0 && x < width && y < height;
    }

    private void checkBounds(int x, int y) {
        if (x < 0 || x >= width) {
            throw new CoordinateOutOfBoundsException("The x-coordinate is out of bounds: x -> " + x + " must be less than " + width);
        }

        if (y < 0 || y >= height) {
            throw new CoordinateOutOfBoundsException("The y-coordinate is out of bounds: y -> " + y + " must be less than " + height);
        }
    }

}
