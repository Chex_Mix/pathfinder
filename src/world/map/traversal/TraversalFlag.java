package world.map.traversal;

import world.map.Direction;

public enum TraversalFlag {
    WALK_NORTH_WEST       ( 0),
    WALK_NORTH            ( 1),
    WALK_NORTH_EAST       ( 2),
    WALK_EAST             ( 3),
    WALK_SOUTH_EAST       ( 4),
    WALK_SOUTH            ( 5),
    WALK_SOUTH_WEST       ( 6),
    WALK_WEST             ( 7),
    WALK_BLOCKED          ( 8),

    PROJECTILE_NORTH_WEST ( 9),
    PROJECTILE_NORTH      (10),
    PROJECTILE_NORTH_EAST (11),
    PROJECTILE_EAST       (12),
    PROJECTILE_SOUTH_EAST (13),
    PROJECTILE_SOUTH      (14),
    PROJECTILE_SOUTH_WEST (15),
    PROJECTILE_WEST       (16),
    PROJECTILE_BLOCKED    (17),

    LANDSCAPE_BLOCKED     (21),
    ;

    private final int flag;

    TraversalFlag(int bit) {
        this.flag = 1 << bit;
    }

    public int getFlag() {
        return flag;
    }

    public TraversalFlag opposite() {
        return opposite(this);
    }

    public static TraversalFlag forDirection(Direction direction) {
        switch (direction) {
            case NORTH_WEST: return WALK_NORTH_WEST;
            case NORTH:      return WALK_NORTH;
            case NORTH_EAST: return WALK_NORTH_EAST;
            case EAST:       return WALK_EAST;
            case SOUTH_EAST: return WALK_SOUTH_EAST;
            case SOUTH:      return WALK_SOUTH;
            case SOUTH_WEST: return WALK_SOUTH_WEST;
            case WEST:       return WALK_WEST;
        }

        throw new UnsupportedOperationException("Direction unsupported: " + direction);
    }

    public static TraversalFlag opposite(TraversalFlag flag) {
        switch (flag) {
            case WALK_NORTH_WEST:   return WALK_SOUTH_EAST;
            case WALK_NORTH:        return WALK_SOUTH;
            case WALK_NORTH_EAST:   return WALK_SOUTH_WEST;
            case WALK_EAST:         return WALK_WEST;
            case WALK_SOUTH_EAST:   return WALK_NORTH_WEST;
            case WALK_SOUTH:        return WALK_NORTH;
            case WALK_SOUTH_WEST:   return WALK_NORTH_EAST;
            case WALK_WEST:         return WALK_EAST;

            case PROJECTILE_NORTH_WEST: return PROJECTILE_SOUTH_EAST;
            case PROJECTILE_NORTH:      return PROJECTILE_SOUTH;
            case PROJECTILE_NORTH_EAST: return PROJECTILE_SOUTH_WEST;
            case PROJECTILE_EAST:       return PROJECTILE_WEST;
            case PROJECTILE_SOUTH_EAST: return PROJECTILE_NORTH_WEST;
            case PROJECTILE_SOUTH:      return PROJECTILE_NORTH;
            case PROJECTILE_SOUTH_WEST: return PROJECTILE_NORTH_EAST;
            case PROJECTILE_WEST:       return PROJECTILE_EAST;

        }

        throw new UnsupportedOperationException("Traversal flag unsupported: " + flag);
    }

}
