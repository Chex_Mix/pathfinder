package world.map.traversal.path.pathfinder;

import world.map.Direction;
import world.map.Position;
import world.map.traversal.path.Path;
import world.map.traversal.path.PathFinder;
import world.map.traversal.TraversalManager;

import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import static world.map.Direction.*;

public class BFSPathfinder extends PathFinder {

    private static final Direction[] BFS_ORDER = {
        WEST,
        EAST,
        SOUTH,
        NORTH,

        SOUTH_WEST,
        SOUTH_EAST,
        NORTH_WEST,
        NORTH_EAST
    };

    public BFSPathfinder(TraversalManager traversalManager) {
        super(traversalManager);
    }

    @Override
    public Path findPath(Position origin, Position destination, int radius) {
        Set<Position> visited = new HashSet<>();
        Deque<Node> nodes = new LinkedList<>();

        Node closestNode = new Node(origin, null);
        nodes.add(closestNode);
        visited.add(origin);

        int closestLength = origin.distanceSq(destination);

        while (!nodes.isEmpty()) {
            Node node = nodes.poll();
            Position position = node.getPosition();

            if (position.equals(destination)) {
                closestNode = node;
                break;
            }

            int length = position.distanceSq(destination);
            if (length < closestLength) {
                closestNode = node;
                closestLength = length;
            }

//            if (closestLength == radius) {
//                closestNode = node;
//                break;
//            }

            for (Direction direction : BFS_ORDER) {
                if (!position.canTransform(direction)) {
                    continue;
                }

                Position transformed = position.transform(direction);

                if (visited.contains(transformed)) {
                    continue;
                }

                if (!traversalManager.getMatrix().inBounds(transformed.getX(), transformed.getY())) {
                    continue;
                }

                if (traversable(position, direction)) {
                    nodes.add(new Node(transformed, node));
                    visited.add(transformed);
                }
            }
        }

        Deque<Position> path = new LinkedList<>();

        Node next = closestNode;
        while (next.hasParent()) {
            path.addFirst(next.getPosition());
            next = next.getParent();
        }

        return new Path(path);
    }

}

