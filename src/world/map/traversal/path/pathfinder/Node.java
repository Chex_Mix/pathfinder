package world.map.traversal.path.pathfinder;

import world.map.Position;

public class Node {
    private final Position position;
    private final Node parent;

    public Node(Position position, Node parent) {
        this.position = position;
        this.parent = parent;
    }

    public Position getPosition() {
        return position;
    }

    public Node getParent() {
        return parent;
    }

    public boolean hasParent() {
        return parent != null;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (obj instanceof Node) {
            Node other = (Node) obj;
            return position.equals(other.position);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return position.hashCode();
    }

    @Override
    public String toString() {
        return "[position=" + getPosition() + ", hasParent=" + hasParent() + "]";
    }

}
