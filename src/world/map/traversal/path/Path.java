package world.map.traversal.path;

import world.map.Position;

import java.util.Deque;

public final class Path {
    private final Deque<Position> path;

    public Path(Deque<Position> path) {
        this.path = path;
    }

    public Position getOrigin() {
        return path.peekFirst();
    }

    public Position getDestination() {
        return path.peekLast();
    }

    public boolean isPossible() {
        return path != null && !path.isEmpty();
    }

    public Deque<Position> getPath() {
        return path;
    }

}
