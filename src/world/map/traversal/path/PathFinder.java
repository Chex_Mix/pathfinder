package world.map.traversal.path;

import world.map.Direction;
import world.map.Position;
import world.map.traversal.TraversalManager;

public abstract class PathFinder {
    protected final TraversalManager traversalManager;

    protected PathFinder(TraversalManager traversalManager) {
        this.traversalManager = traversalManager;
    }

    public abstract Path findPath(Position origin, Position destination, int radius);

    protected boolean traversable(Position position, Direction direction) {
        int x = position.getX(), y = position.getY();

        switch (direction) {
            case NORTH_WEST:
                return !traversalManager.blockedNorthWest(x, y);
            case NORTH:
                return !traversalManager.blockedNorth(x, y);
            case NORTH_EAST:
                return !traversalManager.blockedNorthEast(x, y);
            case EAST:
                return !traversalManager.blockedEast(x, y);
            case SOUTH_EAST:
                return !traversalManager.blockedSouthEast(x, y);
            case SOUTH:
                return !traversalManager.blockedSouth(x, y);
            case SOUTH_WEST:
                return !traversalManager.blockedSouthWest(x, y);
            case WEST:
                return !traversalManager.blockedWest(x, y);
        }

        throw new UnsupportedOperationException("Direction unsupported: " + direction);
    }

}
