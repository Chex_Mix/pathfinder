package world.map.traversal;

import world.map.Direction;

import java.awt.*;

import static world.map.traversal.TraversalFlag.*;

public class TraversalManager {
    private final TraversalMatrix traversalMatrix;

    public TraversalManager(int width, int height) {
        traversalMatrix = new TraversalMatrix(width, height);
    }

    public void block(int x, int y, int width, int length) {
        for (int dy = 0; dy < length; dy++) {
            for (int dx = 0; dx < width; dx++) {
                flag(x + dx, y + dy, WALK_BLOCKED);
            }
        }
    }

    public void flagWall(int x, int y, boolean projectiles, Direction direction) {
        TraversalFlag flag = TraversalFlag.forDirection(direction);
        Point vector = direction.vector();

        flag(x, y, flag);
        flag(x + vector.x, y + vector.y, flag.opposite());

        if (projectiles) {
            flag(x, y, flag);
            flag(x + vector.x, y + vector.y, flag.opposite());
        }
    }

    public void flag(int x, int y, TraversalFlag flag) {
        traversalMatrix.flag(x, y, flag);
    }

    public boolean flagged(int x, int y, TraversalFlag flag) {
        if (!traversalMatrix.inBounds(x, y)) {
            return false;
        }
        return traversalMatrix.flagged(x, y, flag);
    }

    private boolean anyOf(int x, int y, TraversalFlag... flags) {
        return traversalMatrix.anyOf(x, y, flags);
    }

    public boolean blockedNorthWest(int x, int y) {
        return anyOf(x - 1, y - 1, WALK_BLOCKED, LANDSCAPE_BLOCKED, WALK_SOUTH, WALK_EAST, WALK_SOUTH_EAST)
            || anyOf(x - 1, y,     WALK_BLOCKED, LANDSCAPE_BLOCKED, WALK_EAST)
            || anyOf(x,     y - 1, WALK_BLOCKED, LANDSCAPE_BLOCKED, WALK_SOUTH);
    }

    public boolean blockedNorth(int x, int y) {
        return anyOf(x, y - 1,     WALK_BLOCKED, LANDSCAPE_BLOCKED, WALK_SOUTH);
    }

    public boolean blockedNorthEast(int x, int y) {
        return anyOf(x + 1, y - 1, WALK_BLOCKED, LANDSCAPE_BLOCKED, WALK_SOUTH, WALK_WEST, WALK_SOUTH_WEST)
            || anyOf(x + 1, y,     WALK_BLOCKED, LANDSCAPE_BLOCKED, WALK_WEST)
            || anyOf(x,     y - 1, WALK_BLOCKED, LANDSCAPE_BLOCKED, WALK_SOUTH);
    }

    public boolean blockedEast(int x, int y) {
        return anyOf(x + 1, y,     WALK_BLOCKED, LANDSCAPE_BLOCKED, WALK_WEST);
    }

    public boolean blockedSouthEast(int x, int y) {
        return anyOf(x + 1, y + 1, WALK_BLOCKED, LANDSCAPE_BLOCKED, WALK_NORTH, WALK_WEST, WALK_NORTH_WEST)
            || anyOf(x + 1, y,     WALK_BLOCKED, LANDSCAPE_BLOCKED, WALK_WEST)
            || anyOf(x,     y + 1, WALK_BLOCKED, LANDSCAPE_BLOCKED, WALK_NORTH);
    }

    public boolean blockedSouth(int x, int y) {
        return anyOf(x, y + 1,     WALK_BLOCKED, LANDSCAPE_BLOCKED, WALK_NORTH);
    }

    public boolean blockedSouthWest(int x, int y) {
        return anyOf(x - 1, y + 1, WALK_BLOCKED, LANDSCAPE_BLOCKED, WALK_NORTH, WALK_EAST, WALK_NORTH_EAST)
            || anyOf(x - 1, y,     WALK_BLOCKED, LANDSCAPE_BLOCKED, WALK_EAST)
            || anyOf(x,     y + 1, WALK_BLOCKED, LANDSCAPE_BLOCKED, WALK_NORTH);
    }

    public boolean blockedWest(int x, int y) {
        return anyOf(x - 1, y,     WALK_BLOCKED, LANDSCAPE_BLOCKED, WALK_EAST);
    }

    public TraversalMatrix getMatrix() {
        return traversalMatrix;
    }

}
