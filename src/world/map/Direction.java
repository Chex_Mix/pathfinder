package world.map;

import java.awt.*;

public enum Direction {
    NORTH_WEST (-1,  1),
    NORTH      ( 0,  1),
    NORTH_EAST ( 1,  1),
    EAST       ( 1,  0),
    SOUTH_EAST ( 1, -1),
    SOUTH      ( 0, -1),
    SOUTH_WEST (-1, -1),
    WEST       (-1,  0);

    private final Point vector;

    Direction(int dx, int dy) {
        vector = new Point(dx, -dy);
    }

    public Point vector() {
        return vector;
    }

    public Direction opposite() {
        return opposite(this);
    }

    public static Direction opposite(Direction direction) {
        switch (direction) {
            case NORTH_WEST: return SOUTH_EAST;
            case NORTH:      return SOUTH;
            case NORTH_EAST: return SOUTH_WEST;
            case EAST:       return WEST;
            case SOUTH_EAST: return NORTH_WEST;
            case SOUTH:      return NORTH;
            case SOUTH_WEST: return NORTH_EAST;
            case WEST:       return EAST;
        }

        throw new UnsupportedOperationException("Direction unsupported: " + direction);
    }

}
