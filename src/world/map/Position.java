package world.map;

import java.awt.*;

/**
 * A {@code Position} is a vector in two dimensions.
 *
 * @author Michael Maksoudian
 */
public final class Position {

    /** The hashed (x, y) coordinates. */
    private int hash;

    /**
     * Constructs a new {@link Position} object by hashing the valid (x, y) coordinate into an integer.
     *
     * @param x the x-coordinate
     * @param y the y-coordinate
     */
    private Position(int x, int y) {
        hash = hash(x, y);
    }

    /**
     * Creates a new {@link Position} object.
     *
     * @param x the x-coordinate, in the domain [0, 65535]
     * @param y the y-coordinate, in the domain [0, 65535]
     * @return a new {@link Position} object
     * @throws CoordinateOutOfBoundsException if the (x, y) coordinate is out of the domain
     */
    public static Position create(int x, int y) {
        checkBounds(x, y);
        return new Position(x, y);
    }

    /** @return the x-coordinate */
    public int getX() {
        return hash & 0xFFFF;
    }

    /** @return the y-coordinate */
    public int getY() {
        return hash >> 16 & 0xFFFF;
    }

    public boolean canTransform(Direction direction) {
        Point vector = direction.vector();
        int x = getX() + vector.x;
        int y = getY() + vector.y;
        return x >= 0 && y >= 0 && x <= 0xFFFF && y <= 0xFFFF;
    }

    public Position transform(Direction direction) {
        Point vector = direction.vector();
        int x = getX() + vector.x;
        int y = getY() + vector.y;
        return create(x, y);
    }

    public int distanceSq(Position other) {
        int dx = other.getX() - getX();
        int dy = other.getY() - getY();
        return dx * dx + dy * dy;
    }

    public Position northWest() {
        return transform(Direction.NORTH_WEST);
    }

    public Position north() {
        return transform(Direction.NORTH);
    }

    public Position northEast() {
        return transform(Direction.NORTH_EAST);
    }

    public Position east() {
        return transform(Direction.EAST);
    }

    public Position southEast() {
        return transform(Direction.SOUTH_EAST);
    }

    public Position south() {
        return transform(Direction.SOUTH);
    }

    public Position southWest() {
        return transform(Direction.SOUTH_WEST);
    }

    public Position west() {
        return transform(Direction.WEST);
    }

    public static int hash(int x, int y) {
        return (y & 0xFFFF) << 16 | (x & 0xFFFF);
    }

    private static void checkBounds(int x, int y) {
        if (x < 0 || y < 0) {
            throw new CoordinateOutOfBoundsException("Position coordinates must be non-negative, (x, y) -> (" + x + ", " + y + ")");
        }

        if (x > 0xFFFF || y > 0xFFFF) {
            throw new CoordinateOutOfBoundsException("XY-coordinate must be an integer in the set [0, 65535], (x, y) -> (" + x + ", " + y + ")");
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (obj instanceof Position) {
            Position other = (Position) obj;
            return hash == other.hash;
        }

        return false;
    }

    @Override
    public int hashCode() {
        return hash;
    }

    @Override
    public String toString() {
        return "[x=" + getX() + ", y=" + getY() + "]";
    }

}
