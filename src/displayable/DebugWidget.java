package displayable;

import com.engine.util.Stopwatch;

import java.awt.*;

public class DebugWidget {
    private final Stopwatch debugTimer = Stopwatch.create();
    private final String[] lines = new String[4];
    private int frames;
    private int ups;

    private final int x, y, padding;
    private int borderWidth, borderHeight, stringHeight;
    private boolean changed;

    public DebugWidget(int x, int y) {
        this.x = x;
        this.y = y;
        padding = 5;
    }

    public void update() {
        ups++;

        if (!debugTimer.hasElapsed(1000)) {
            return;
        }

        Runtime runtime = Runtime.getRuntime();
        long used = runtime.totalMemory() - runtime.freeMemory();
        set(0, "FPS :: " + frames);
        set(1, "UPS :: " + ups);
        set(2, "Alloc :: " + (runtime.maxMemory() >> 20) + " MB");
        set(3, "Mem :: " + (used >> 20) + " MB [ " + 100 * used / runtime.maxMemory() + "% ]");
        changed = true;

        debugTimer.reset();
        frames = 0;
        ups = 0;
    }

    public void render(Graphics2D graphics) {
        frames++;

        if (changed) {
            FontMetrics metrics = graphics.getFontMetrics();
            calculateBorderSize(metrics);
            changed = false;
        }

        graphics.setColor(Color.DARK_GRAY);
        graphics.fillRoundRect(x, y, borderWidth + 2 * padding, borderHeight + padding, 7, 7);

        graphics.setColor(Color.BLACK);
        graphics.drawRoundRect(x, y, borderWidth + 2 * padding, borderHeight + padding, 7, 7);

        for (int index = 0; index < lines.length; index++) {
            String line = lines[index];

            if (line == null) {
                line = "---";
            }

            int lineX = x + padding;
            int lineY = y + (padding + stringHeight) * (index + 1);

            graphics.setColor(Color.BLACK);
            graphics.drawString(line, lineX + 1, lineY + 1);

            graphics.setColor(Color.YELLOW);
            graphics.drawString(line, lineX, lineY);
        }
    }

    private void calculateBorderSize(FontMetrics metrics) {
        stringHeight = metrics.getMaxAscent() - metrics.getMaxDescent();
        borderHeight = padding + stringHeight;

        for (String line : lines) {
            int lineWidth = metrics.stringWidth(line);

            if (borderWidth < lineWidth) {
                borderWidth = lineWidth;
            }
        }

        borderHeight *= lines.length;
    }

    public void set(int index, String string) {
        if (string == null) {
            lines[index] = null;
            changed = true;
            return;
        }

        if (!string.equals(lines[index])) {
            lines[index] = string;
            changed = true;
        }
    }

}
