package parser;

import world.map.traversal.TraversalFlag;
import world.map.traversal.TraversalMatrix;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class ParseFile {

    private final TraversalMatrix matrix;

    public ParseFile(TraversalMatrix matrix) {
        this.matrix = matrix;
    }

    public void parse() {
        Path path = Paths.get("sandbox/res/region_dump.txt");

        try {
            Scanner scanner = new Scanner(path);
            int y = 0;
            while (scanner.hasNext()) {
                String[] xCoords = scanner.nextLine().split(" ");

                int x = 0;
                for (String coord : xCoords) {
                    int flag = Integer.parseInt(coord);
                    matrix.flag(x, 103 - y, flag);
                    if (x > 0 && 103 - y > 0 && x < 103 && 103 - y < 103 && matrix.get(x, 103-y) != 0 && !matrix.anyOf(x, 103 - y, TraversalFlag.values())) {
                        System.out.println(x + " " + (103 - y) + " 0b" + Integer.toBinaryString(matrix.get(x, 103-y)));
                    }
//                    if (x > 0 && 103 - y > 0 && x < 103 && 103 - y < 103 && matrix.flagged(x, 103 - y, 0x40000)) {
//                        System.out.println(x + " " + (103 - y) + " 0b" + Integer.toBinaryString(matrix.get(x, 103-y)));
//                    }
                    x++;
                }
                y++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
