package parser;

import world.map.Direction;
import world.map.Position;
import world.map.traversal.TraversalFlag;
import world.map.traversal.TraversalMatrix;
import world.map.traversal.path.pathfinder.Node;

import java.awt.*;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;
import java.util.stream.IntStream;

public class MazeGenerator {

    private static final Direction[] DIRECTIONS = {
        Direction.NORTH,
        Direction.EAST,
        Direction.SOUTH,
        Direction.WEST
    };

    public static void generate(TraversalMatrix matrix) {
        Set<Position> visited = new HashSet<>();
        Position origin = Position.create(0, 0);

        matrix.setAll(0b1010_1010);

        Node node = new Node(origin, null);
        visited.add(origin);

        while (visited.size() != matrix.getWidth() * matrix.getHeight()) {
            Position position = node.getPosition();

            Direction direction = findDirection(visited, matrix, position);

            if (direction == null) {
                if (!node.hasParent()) {
                    break;
                }

                node = node.getParent();
                continue;
            }

            TraversalFlag flag = TraversalFlag.forDirection(direction);
            Position next = position.transform(direction);

            matrix.unflag(position.getX(), position.getY(), flag);
            matrix.unflag(next.getX(), next.getY(), flag.opposite());

            node = new Node(next, node);
            visited.add(next);
        }
    }

    private static Direction findDirection(Set<Position> visited, TraversalMatrix matrix, Position position) {
        Stack<Integer> unchecked = new Stack<>();
        IntStream.range(0, DIRECTIONS.length).forEach(unchecked::add);
        Collections.shuffle(unchecked);

        while (!unchecked.isEmpty()) {
            Direction direction = DIRECTIONS[unchecked.pop()];

            Point vector = direction.vector();
            int x = position.getX() + vector.x;
            int y = position.getY() + vector.y;

            if (!matrix.inBounds(x, y)) {
                continue;
            }

            Position neighbor = position.transform(direction);

            if (visited.contains(neighbor)) {
                continue;
            }

            return direction;
        }

        return null;
    }

}
