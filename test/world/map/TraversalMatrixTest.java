package world.map;

import org.junit.jupiter.api.Test;
import world.map.traversal.TraversalFlag;
import world.map.traversal.TraversalMatrix;

import static org.junit.jupiter.api.Assertions.*;

class TraversalMatrixTest {

    @Test
    void flagTest() {
        TraversalMatrix map = new TraversalMatrix(1, 1);

        // test adding multiple flags
        map.flagAll(0, 0, TraversalFlag.WALK_NORTH, TraversalFlag.WALK_EAST, TraversalFlag.WALK_SOUTH_WEST);
        assertEquals(TraversalFlag.WALK_NORTH.getFlag() | TraversalFlag.WALK_EAST.getFlag() | TraversalFlag.WALK_SOUTH_WEST.getFlag(), map.get(0, 0));

        // test adding a single flag
        map.flagAll(0, 0, TraversalFlag.WALK_NORTH_EAST);
        assertEquals(TraversalFlag.WALK_NORTH_EAST.getFlag() | TraversalFlag.WALK_NORTH.getFlag() | TraversalFlag.WALK_EAST.getFlag() | TraversalFlag.WALK_SOUTH_WEST.getFlag(), map.get(0, 0));

        // test removing existing flags
        map.unflagAll(0, 0, TraversalFlag.WALK_NORTH, TraversalFlag.WALK_NORTH_EAST);
        assertEquals(TraversalFlag.WALK_EAST.getFlag() | TraversalFlag.WALK_SOUTH_WEST.getFlag(), map.get(0, 0));

    }

}